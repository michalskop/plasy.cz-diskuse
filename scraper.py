"""Scraper for diskuse on plasy.cz."""

import csv
from datetime import datetime
from lxml import html
import re
import requests

path = ""

articles = []

# First run to get number of pages
url0 = "http://www.plasy.cz/mesto/ostatni/diskuze/?diskuze=WG9%2BMzk3fjB%2Bc2hvd34wfm9Z&zobraz_vse=Zobrazit+v%C5%A1e"
r = requests.get(url0)
r.encoding = 'utf-8'
root = html.fromstring(r.text)
ass = root.xpath("//div[@class='per_page']/ul/li/a")
for a in ass:
    last = a.text

for i in range(1, int(last) + 1):
    url = "http://www.plasy.cz/mesto/ostatni/diskuze/?diskuze=WG9%2BMzk3fjB%2Bc2hvd34wfm9Z&zobraz_vse=Zobrazit+v%C5%A1e&dis_pageshowing=" + str(i)
    r = requests.get(url)
    r.encoding = 'utf-8'
    root = html.fromstring(r.text)
    divs = root.xpath("//div[@class='diskuze_detail']")[0]
    for div in divs:
        try:
            dis = div.xpath(".//a/@name")[0]
            match = re.search('([0-9]){1,}', dis)
            dis_id = match.group(0)
            header = div.xpath(".//div[@class='diskuze_hlavicka']")[0].text_content()
            start = header.find("\n\t\t")
            end = header.find("")
            breaks = re.finditer("(\n\t\t)", header)
            positions = []
            iter = 0
            fr = None
            for br in breaks:
                if iter > 0:
                    positions.append({'from': fr, 'to': br.span()[0]})
                fr = br.span()[1]
                iter += 1
            positions.append({"from": fr, "to": len(header) - 1})
            author = header[positions[0]['from'] + 4:positions[0]['to']].strip()
            ip = header[positions[1]['from'] + 4:positions[1]['to'] - 1].strip()
            date_cs = header[positions[2]['from'] + 7:positions[2]['to']].strip()
            date = datetime.strptime(date_cs, "%d.%m.%Y %H:%M:%S").isoformat()
            subject = header[positions[3]['from'] + 9:positions[3]['to']].strip()
            body = div.xpath(".//div[@class='diskuze_telo']")[0].text_content()
            article = {
                'id': dis_id,
                'author': author,
                'ip': ip,
                'subject': subject,
                'date': date,
                'body': body
            }
            articles.append(article)
        except Exception:
            nothing = 0

# path = ''
existing = []
try:
    with open(path + "articles.csv") as fin:
        dr = csv.DictReader(fin)
        for row in dr:
            existing.append(row['id'])
except Exception:
    with open(path + "articles.csv", "w") as fout:
        header = ['id', 'author', 'ip', 'date', 'subject', 'body']
        dw = csv.DictWriter(fout, header)
        dw.writeheader()

updating = False
with open(path + "articles.csv", "a") as fout:
    header = ['id', 'author', 'ip', 'date', 'subject', 'body']
    dw = csv.DictWriter(fout, header)
    for article in articles:
        if article['id'] not in existing:
            dw.writerow(article)
            # bot send me:
            message = "New discussion plasy.cz: " + article['author'] + " " + article['subject'] + ": " + article['body'][0:60] + "( " + "http://www.plasy.cz/mesto/ostatni/diskuze/?zobraz_vybrane=Zobrazit+vybrané&diskuze=WG9%2BMzk3fjB%2Bc2hvd34wfm9Z&" + article['id'] + "=start)"
            params = {'message': message}
            requests.get("https://botsend.me", params=params)
            updating = True
